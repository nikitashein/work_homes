from django.contrib import admin
from django.urls import path, include

from djanga import views

#Изменяю для гита

urlpatterns = [
    path('geo/', include('georgydud.urls')),
    path('', include('djanga.urls')),
    path('admin/', admin.site.urls)
]
