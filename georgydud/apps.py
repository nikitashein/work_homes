from django.apps import AppConfig


class GeorgydudConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'georgydud'
