from django.urls import path

from . import views

urlpatterns = [
    path('<args>/', views.args)
]
