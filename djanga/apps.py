from django.apps import AppConfig


class DjangaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djanga'
